package coinpurse;

import java.util.ResourceBundle;

/**
 * Encapsulate operation of creating money objects.
 * @author Jidapar Jettananurak.
 */
public abstract class MoneyFactory {
	private static MoneyFactory instance = null;

	protected MoneyFactory() {
	}

	/** Get the money factory instance. */
	public static MoneyFactory getInstance() { 
		if(instance == null){
			ResourceBundle bundle = ResourceBundle.getBundle( "purse" );
			String value = bundle.getString( "moneyfactory" ); 

			MoneyFactory factory;

			if(value != null){
				System.out.println("Factory class is " + value); // testing
				try {
					factory = (MoneyFactory)Class.forName(value).newInstance();
				} catch (InstantiationException | IllegalAccessException
						| ClassNotFoundException  ex) {
					System.out.println("Error creating MoneyFactory "+ex.getMessage() );
					return instance = new ThaiMoneyFactory();
				}
			}else
				factory = MoneyFactory.getInstance();
		}
		return instance;
	}

	public void setMoneyFactory(MoneyFactory newMoneyFactory){
		instance = newMoneyFactory;
	}

	/**
	 * Return a money object that represents the value
	 * of the String parameter.
	 * @param amount is amount of money or some description, e.g. "red" for coupon
	 * @return Valuable object for the amount.
	 * What should you return if the parameter isn't valid?
	 * For example, "purple" or "17"?
	 */
	public Valuable createMoney(String value)  {

		return createMoney( Double.parseDouble(value) );

	}

	public abstract String getCurrencyCoin();
	public abstract String getCurrencyBankNote();
	public abstract Valuable createMoney(double value);
}