package coinpurse;

/**
 * 
 * @author Jidapar Jettananurak
 *
 */
public interface Valuable {
	
	/**
	 * @return value of coin
	 */
	public double getValue();
	
}
