package coinpurse;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Font;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
/**
 * PurseBalance will show money in purse.
 * @author Jidapar Jettananurak
 *
 */
public class PurseBalance extends JFrame implements Observer{

	private Container board;
	private JPanel background;
	private JLabel label;
	
	/**
	 * Constructor create frame.
	 */
	public PurseBalance(){
		super("Purse Balance");
		initComponents();
	}
	
	/**
	 * This method will update object in purse.
	 */
	public void update(Observable subject, Object info) {
		if(subject instanceof Purse){
			Purse purse = (Purse)subject;
			int balance = (int) purse.getBalance();
			label.setText(balance + " baht");
		}
		
		if(info != null)
			System.out.println(info);
	}
	
	/**
	 * initComonents create pane for show information.
	 */
	public void initComponents(){
		background = new JPanel();
		this.setLayout(new FlowLayout());
		Font font = new Font("Arial",Font.BOLD,30);
		label = new JLabel("0 baht");
		label.setFont(font);
		background.add(label);
		this.add(background);
	}
	
	/**
	 * This method will run by open frame.
	 */
	public void run(){
		this.setVisible(true);
		this.pack();
	}
	
}
