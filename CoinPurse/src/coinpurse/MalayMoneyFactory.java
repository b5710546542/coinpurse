package coinpurse;
/**
 * Class money of Malaysia.
 * @author Jidapar Jettananurak.
 *
 */
public class MalayMoneyFactory extends MoneyFactory{

	public Valuable createMoney(double value) throws IllegalArgumentException{
		if(value == 0.05) return new Coin(5);
		else if(value == 0.10) return new Coin(10);
		else if(value == 0.20) return new Coin(20);
		else if(value == 0.50) return new Coin(50);
		else if(value == 1) return new BankNote(1);
		else if(value == 2) return new BankNote(2);
		else if(value == 5) return new BankNote(5);
		else if(value == 10) return new BankNote(10);
		else if(value == 20) return new BankNote(20);
		else if(value == 500) return new BankNote(500);
		else if(value == 100) return new BankNote(100);
		
		throw new IllegalArgumentException();
	}

	/**
	 * get currency of coin.
	 */
	public String getCurrencyCoin(){
		return "Sen";
	}
	
	/**
	 * get currency of banknote.
	 */
	public String getCurrencyBankNote(){
		return "Ringgit";
	}
	
}
