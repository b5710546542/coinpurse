package coinpurse;

import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Font;
import java.util.Observable;
import java.util.Observer;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
/**
 * PurseStatus work by show status of purse 
 * @author Jidapar Jettananurak
 *
 */
public class PurseStatus extends JFrame implements Observer{

	private Container board;
	private JPanel background;
	private JLabel label;
	private JProgressBar bar;
	
	/**
	 * Constructor create frame.
	 */
	public PurseStatus(){
		super("Purse Status");
		initComponents();
	}
	
	/**
	 * This method will update object in purse.
	 */
	public void update(Observable subject, Object info) {
		
		if(subject instanceof Purse){
			Purse purse = (Purse)subject;
			bar.setMinimum(0);
			bar.setMaximum(purse.getCapacity());
			//bar = new JProgressBar(0,purse.getCapacity());
			if(purse.isFull())
				label.setText("FULL");
			else
				label.setText(purse.count()+"");
			
			bar.setValue(purse.count());
		}
		
		if(info != null)
			System.out.println(info);
	}
	
	/**
	 * initComonents create pane for show information.
	 */
	public void initComponents(){
		bar = new JProgressBar();
		background = new JPanel();
		this.setLayout(new FlowLayout());
		Font font = new Font("Arial",Font.BOLD,30);
		label = new JLabel("0 baht");
		label.setFont(font);
		background.add(label);
		background.add(bar);
		this.add(background);
		
	}
	
	/**
	 * This method will run by open frame.
	 */
	public void run(){
		this.setVisible(true);
		this.pack();
	}
	
}
