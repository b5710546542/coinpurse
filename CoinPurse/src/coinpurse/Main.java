
package coinpurse;

import coinpurse.strategy.GreedyWithdraw;
import coinpurse.strategy.RecursiveWithdraw;
 

/**
 * A main class to create objects and connect objects together.
 * The user interface needs a reference to coin purse.
 * @author Jidapar Jettananurak.
 */
public class Main {

    /**
     * @param args not used
     */
    public static void main( String[] args ) {
    	
    	

        Purse purse = new Purse(20);
//        purse.insert(new Coin(101));
//        purse.insert(new Coin(97));
//        purse.insert(new Coin(62));
//        purse.insert(new Coin(30));
//        purse.insert(new Coin(21));
//        purse.insert(new Coin(19));
//        purse.insert(new Coin(19));
//        purse.insert(new Coin(17));
//        purse.insert(new Coin(8));
//        purse.insert(new Coin(7));
//        purse.insert(new Coin(2));
//        System.out.println("asdasd");
//        purse.setWithdrawStrategy(new RecursiveWithdraw());
//        
//        Valuable[] vv = purse.withdraw(2);
//        if(vv==null)
//        	System.out.println("vv is null.");
//        else
//        	for(Valuable v : vv){
//        	System.out.println(v);
//        }
        
//        purse.setWithdrawStrategy(new GreedyWithdraw());
        purse.setWithdrawStrategy(new RecursiveWithdraw());
        
        PurseStatus ps = new PurseStatus();
        PurseBalance po = new PurseBalance();
       
       purse.addObserver(po);
       purse.addObserver(ps);
        
        ConsoleDialog ui = new ConsoleDialog( purse );
        po.run();
        ps.run();
        ui.run();
    }
}
