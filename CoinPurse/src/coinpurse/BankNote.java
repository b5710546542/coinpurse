package coinpurse;
/**
 * A banknote with a monetary value.
 * @author Jidapar Jettananurak
 *
 */
public class BankNote extends AbstractValuable{

	/**value of BankNote.*/
	private double value;
	private String currency;
	private MoneyFactory instance = MoneyFactory.getInstance();

	/**serialNumber that started with 100000.*/
	private static long serialNumber = 999999;

	/**serialNumber in next.*/
	private long nextSerialNumber;

	/**
	 * Constructor to create new BankNote.
	 * @param value is the value of BankNote
	 */
	public BankNote(double value){
		serialNumber++;
		nextSerialNumber = serialNumber++;
		this.value = value;
		this.currency = instance.getCurrencyBankNote();
	}

	/**
	 * SerialNumber in a next.
	 * @return nextSerialNumber
	 */
	public long getNextSerialNumber(){
		return nextSerialNumber;
	}

	/**@return value of BankBote.*/
	public double getValue(){
		return value;
	}

	/**
	 * define object.
	 * @return value of BankNote
	 */
	public String toString(){
		return String.format("%.0f %s Banknote" , this.getValue() , this.currency);
	}

}
