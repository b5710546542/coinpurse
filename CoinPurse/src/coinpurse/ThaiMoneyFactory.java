package coinpurse;
/**
 * Class money of Thailand.
 * @author Jidapar Jettananurak.
 *
 */
public class ThaiMoneyFactory extends MoneyFactory{

	public Valuable createMoney(double value) throws IllegalArgumentException {

		if(value == 1) return new Coin(1);
		else if(value == 2) return new Coin(2);
		else if(value == 5) return new Coin(5);
		else if(value == 10) return new Coin(10);
		else if(value == 20) return new BankNote(20);
		else if(value == 50) return new BankNote(50);
		else if(value == 100) return new BankNote(100);
		else if(value == 500) return new BankNote(500);
		else if(value == 1000) return new BankNote(1000);
		
		throw new IllegalArgumentException();
		
	}
	
	/**
	 * get currency of coin.
	 */
	public String getCurrencyCoin(){
		return "Baht";
	}
	
	/**
	 * get currency of banknote.
	 */
	public String getCurrencyBankNote(){
		return "Baht";
	}

}
