package coinpurse;
 
/**
 * A coin with a monetary value.
 * You can't change the value of a coin.
 * @author Jidapar Jettananurak
 */
public class Coin extends AbstractValuable{

    /** Value of the coin. */
    private double value;
    private String currency;
    private MoneyFactory instance = MoneyFactory.getInstance();
    
    /** 
     * Constructor for a new coin. 
     * @param value is the value for the coin
     */
    public Coin( double value ) {
       this.value = value;
       this.currency = instance.getCurrencyCoin();
    }

    /** @return the value of coin.*/
    public double getValue(){
    	return this.value;
    }
    
   /**
    * define object.
    * @return value of coin
    */
    public String toString(){
//    	if(instance instanceOf )
    	return String.format("%.0f %s Coin" , this.getValue() , this.currency);
    }

}

