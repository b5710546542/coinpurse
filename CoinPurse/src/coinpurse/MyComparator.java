package coinpurse;

import java.util.Comparator;

/**
 * MyComparator can compare any thing that have value.
 * @author Jidapar Jettananurak
 *
 */
public class MyComparator implements Comparator<Valuable>{

	/**
	 * Compare a and b.
	 * @param a have valuable
	 * @param b have valuable
     * @return < 0 if a should be "before" b,
     *         > 0 if a should be "after" b,
	 *         = 0 if a and b have same lexical order.
	 */
	public int compare(Valuable a, Valuable b){
		if(a.getValue() > b.getValue()){
			return 1;
		}
		else if(a.getValue() < b.getValue()){
			return -1;
		}
		else
			return 0;
	}

}
