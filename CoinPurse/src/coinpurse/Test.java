package coinpurse;

import java.util.ResourceBundle;

/**
 * test.
 * @author Jidapar Jettananurak
 *
 */
public class Test {
	/**
	 * Main.
	 * @param arg argument.
	 */
	public static void main (String [] arg){

		//		MoneyFactory factore = MoneyFactory.getInstance();
		//		Purse purse = new Purse( 5 );
		//		Coin coin1 = new Coin( 10 );
		//		Coupon c   = new Coupon("blue");
		//		System.out.println(purse.insert( coin1 ));
		//		System.out.println(purse.insert( c ));
		//		System.out.println(purse.getBalance());
		//		System.out.println(purse.count());
		//		Valuable[] stuff = purse.withdraw(60);
		//		System.out.println(stuff[0].toString());
		//		System.out.println(stuff[1].toString());
		//		System.out.println(purse.getBalance());






		MoneyFactory factory = MoneyFactory.getInstance();
		Valuable m = factory.createMoney(5);
		System.out.println(m.toString());
		Valuable m2 = factory.createMoney("1000.0");
		System.out.println(m2.toString());
		//Valuable m3 = factory.createMoney(0.05);
		//System.out.println(m3.toString());

		factory.setMoneyFactory(new MalayMoneyFactory());
		factory = MoneyFactory.getInstance();
		Valuable m5 = factory.createMoney(10);
		System.out.println(m5.toString());
		Valuable m6 = factory.createMoney("0.05");
		System.out.println(m6.toString());

	}

}
