package coinpurse;
/**
 * 
 * @author Jidapar Jettananurak
 *
 */
public abstract class AbstractValuable implements Valuable{
	
	/**
	 * Compare value between two value.
	 * @param valuable value that want to compare
	 * @return 0 if value are equals, positive value if value are more than valuable, negative value if value are less than valuable
	 */
	public int compareTo(Valuable valuable){
		if(valuable == null)
			return -1;
		
		if(this.getValue() < valuable.getValue())
			return -1;
		else if(this.getValue() > valuable.getValue())
			return 1;
		else
			return 0;
	}
	
	/**
	 * Compare two object are same type and same value.
	 * @param obj is another object to compare to this one
	 * @return true if the type and value of object are same value ,false otherwise.
	 */
	public boolean equals(Object obj){
		if(obj == null)
			return false;
		
		if(this.getClass() != obj.getClass())
			return false;
		
		Valuable other = (Valuable)obj;

		return this.getValue() == other.getValue();	
	}
	
}
