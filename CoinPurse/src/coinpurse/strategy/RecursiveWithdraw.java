package coinpurse.strategy;

import java.util.ArrayList;
import java.util.List;

import coinpurse.Valuable;
/**
 * RecursiveWithdraw can all withdraw case but use memories less than GreedeWithdraw.
 * @author Jidapar Jettananurak
 *
 */
public class RecursiveWithdraw implements WithdrawStrategy{
	
	/**
	 * withdraw is a method withdraw from purse.
	 * @param amount is number of value that want to withdraw
	 * @param valuables is money in purse
	 * @return money that withdraw.
	 */
	public Valuable[] withdraw(double amount, List<Valuable> valuables) {
		List<Valuable> result = withdrawFrom(amount , valuables , new ArrayList<Valuable>() );
		if(result ==null)return null;
		Valuable[] arr = new Valuable[result.size()];
		result.toArray(arr);
		
		List<Valuable> test = new ArrayList<Valuable>(valuables);
		
		for(int j = 0 ; j < result.size() ; j++){
			for(int i = 0 ; i < test.size() ; i++){
				if(arr[i].getValue() == test.get(j).getValue()){
					valuables.remove(test.get(j));
				}
			}
		}
		for(int i=0 ; i < arr.length ; i++){
			System.out.println(arr[i]);
		}
		return arr;
	}
	
	/**
	 * withdrawFrom is helper method it use recursion in withdraw.
	 * @param amount is number of value that want to withdraw
	 * @param valuables is money in purse
	 * @param keep is List of money that can withdraw
	 * @return keep if can withdraw otherwise null
	 */
	public List<Valuable> withdrawFrom(double amount, List<Valuable> valuables , List<Valuable> keep){
		if(amount == 0) return keep;
		if(amount > 0 && valuables.size() == 0) return null;
		if(amount < 0) return null;
		
		keep.add(valuables.get(0));
		List<Valuable>temp = withdrawFrom(amount-valuables.get(0).getValue() , valuables.subList(1, valuables.size()) , keep);
		if(temp == null){
			keep.remove(keep.size()-1);
			keep = withdrawFrom(amount , valuables.subList(1,valuables.size()) , keep);
		}
		return keep;
	}

}
