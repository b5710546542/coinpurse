package coinpurse.strategy;

import java.util.Collections;
import java.util.List;

import coinpurse.MyComparator;
import coinpurse.Valuable;
/**
 * GreedyWithdraw is a strategy of withdraw.
 * @author Jidapar Jettananurak
 *
 */
public class GreedyWithdraw implements WithdrawStrategy{
	
	private final MyComparator myComparator = new MyComparator();
	
	/**
	 * Withdraw the requested amount of money.
	 * @param amount is the amount to withdraw
	 * @param money is the money in purse
	 * @return array of objects for money withdrawn
	 */
	public Valuable[] withdraw( double amount , List<Valuable> money) {

		Collections.sort(money,myComparator);
		
		if ( amount <= 0 ) return null;
		
		/**
		 * purse
		 */
		Valuable []templist = new Valuable[money.size()];
		
		int check = 0;
		double temp = 0;
		double checkAmount = amount;


		if ( amount > 0 )
		{	
			int a = 0;
			for(int i = money.size()-1 ; i >= 0 ; i--){

				if(checkAmount-money.get(i).getValue() >= 0){
					templist[a] = money.get(i);
					temp += money.get(i).getValue();
					checkAmount -= temp;
					a++;
				}
			}

			if(temp == amount){
				for(int i = 0;i < templist.length ; i++){
					for(int j = money.size()-1 ; j >=0 ; j--){
						if(templist[i] == null)
							break;
						if(money.get(j).getValue() == templist[i].getValue()){
							money.remove(j);
							check++;
							break;
						}
					}
				}
				Valuable [] returnList = new Valuable[check];
				for(int i =0; i<check;i++)
				{
					if(templist[i]==null)
						break;
					returnList[i]=templist[i];
				}
				return returnList;
			}
		}
		return null;
	}


}
