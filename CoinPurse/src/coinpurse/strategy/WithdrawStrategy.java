package coinpurse.strategy;

import java.util.List;

import coinpurse.Valuable;
/**
 * WithdrawStrategy is interface for withdraw in purse.
 * @author Jidapar Jettananurak
 *
 */
public interface WithdrawStrategy {
	
	/**
	 * the strategy of withdraw.
	 * @param amount is money that want withdraw
	 * @param valuables is list of purse
	 * @return array of objects
	 */
	public Valuable[] withdraw(double amount, List<Valuable> valuables);

}
