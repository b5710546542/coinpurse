package coinpurse;

import java.util.Comparator;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Observable;

import coinpurse.strategy.GreedyWithdraw;
import coinpurse.strategy.RecursiveWithdraw;
import coinpurse.strategy.WithdrawStrategy;
/**
 *  A coin purse contains coins.
 *  You can insert coins, withdraw money, check the balance,
 *  and check if the purse is full.
 *  When you withdraw money, the coin purse decides which
 *  coins to remove.
 *  
 *  @author Jidapar Jettananurak
 */
public class Purse extends Observable{
	/** Collection of coins in the purse. */
	/** purse can keep any coin. */
	private List<Valuable> money;
	
	private WithdrawStrategy strategy;
	
	/** Capacity is maximum NUMBER of coins the purse can hold.
	 *  Capacity is set when the purse is created.
	 */
	private int capacity;

	/** MyComparator is compare the value.*/
	//private final MyComparator myComparator = new MyComparator();

	/**valuable is array that keep the money from withdraw.*/
	private Valuable[]valuable;

	/** 
	 *  Create a purse with a specified capacity.
	 *  @param capacity is maximum number of coins you can put in purse.
	 */
	public Purse( int capacity ) {

		money = new ArrayList();
		this.capacity = capacity;
	}

	/**
	 * Count and return the number of coins in the purse.
	 * This is the number of coins, not their value.
	 * @return the number of coins in the purse
	 */
	public int count() { return money.size(); }

	/** 
	 *  Get the total value of all items in the purse.
	 *  @return the total value of items in the purse.
	 */
	public double getBalance() {
		double total = 0;
		for(int i = 0 ; i < money.size() ; i++){
			total += money.get(i).getValue();
		}
		return total;
	}


	/**
	 * Return the capacity of the coin purse.
	 * @return the capacity
	 */
	public int getCapacity() { return capacity; }

	/** 
	 *  Test whether the purse is full.
	 *  The purse is full if number of items in purse equals
	 *  or greater than the purse capacity.
	 *  @return true if purse is full
	 */
	public boolean isFull() {
		return money.size() >= capacity;
	}

	/** 
	 * Insert a valuable into the money.
	 * The valuable is only inserted if the money has space for it
	 * and the valuable has positive value.  No worthless valuable!
	 * @param valuable is a Valuable object to insert into money
	 * @return true if valuable inserted, false if can't insert
	 */
	public boolean insert( Valuable valuable ) {

		if(valuable.getValue() == 0) return false;
		if(isFull() == false){
			money.add(valuable);
			
			super.setChanged();
			super.notifyObservers(this);
			
			return true;
		}
		return false;
	}

	/**  
	 *  Withdraw the requested amount of money.
	 *  Return an array of Coins withdrawn from purse,
	 *  or return null if cannot withdraw the amount requested.
	 *  @param amount is the amount to withdraw
	 *  @return array of Coin objects for money withdrawn, 
	 *    or null if cannot withdraw requested amount.
	 */
	public Valuable[] withdraw( double amount ) {
		
		setWithdrawStrategy(new GreedyWithdraw());
		Valuable[] temp = strategy.withdraw(amount, money);
		if(temp == null) {
			setWithdrawStrategy(new RecursiveWithdraw());
			temp = strategy.withdraw(amount, money);
		}
		if(temp != null){
			super.setChanged();
			super.notifyObservers(this);
		}
		
		return temp;
	
	}
	
	/** 
	 * toString returns a string description of the purse contents.
	 * It can return whatever is a useful description.
	 * @return String in money
	 */
	public String toString() {

		String temp = "";

		temp += money.size() + " coins with value " + getBalance();
		return temp;
	}

	/**
	 * setWithdrawSrategy can change strategy of withdraw.
	 * @param strategy is strategy of 
	 */
	public void setWithdrawStrategy(WithdrawStrategy strategy){
		this.strategy = strategy;
	}
}
