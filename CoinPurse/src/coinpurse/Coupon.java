package coinpurse;
import java.util.HashMap;
import java.util.Map;

/**
 * A coupon with a monetary value.
 * @author Jidapar Jettananurak
 *
 */
public class Coupon extends AbstractValuable{
	
	/**	color of Coupon. */
	private String color;
	
	/**	map the coupon with value. */
	static Map<String,Double> map;
	
	static{
		map = new HashMap<String,Double>();
		map.put("red",100.0);
		map.put("blue",50.0);
		map.put("green",20.0);
	}
	
	/**
	 * Constructor of new coupon.
	 * @param color is color of coupon
	 */
	public Coupon(String color){
		this.color = color;
	}
	
	/**
	 * @return value of coupon
	 */
	public double getValue(){
		return map.get(color.toLowerCase());
	}
	
	/** @return String that is color of coupon*/
	public String toString(){
		return this.color + " coupon";
	}

}
